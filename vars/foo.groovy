import groovy.json.*

def call(){
    pipeline {
        agent any
        parameters {
            string(
                name: "NomManifest",
                defaultValue: "_manifest_",
                description: "Saisir ici le nom du fichier manifest dde la branche"
            )
        }

        stages{
            stage("INIT_ENV_VARS"){
                steps{
                    echo "INIT_ENV_VARS"
                    echo "Your param is ${params.NomManifest}"
                }
            }

            stage("CHECKOUT_APP2"){
                steps{
                    echo "CHECKOUT APP"
                    sh 'ls -l'
                }
            }

            stage("BUILD_DBB"){
                steps{
                    echo "BUILD DBB"
                }
            }
        }
    }
}
